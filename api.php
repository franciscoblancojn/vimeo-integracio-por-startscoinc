<?php
class VISS_api{
    private $URL = "https://api.vhx.tv/";

    /**
     * Constructor del api
     *
     * @access public
     * @return void
     */
    public function __construct($atts = array())
    {

    }
    /**
     * request metodo para hacer peticiones al api
     * 
     * @access private
     * @return array
     */
    private function request($POSTFIELDS = array() , $url = "" , $method = "POST" )
    { 
        $curl = curl_init();
        
        $arrayCurl = array(
            CURLOPT_URL => $this->URL.$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $POSTFIELDS,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic '.base64_encode(get_option( 'vimeoApy_apyKey' ).":"),
            ),
        );

        if(count($POSTFIELDS)>0){
            $arrayCurl[CURLOPT_POSTFIELDS] = $POSTFIELDS;
        }

        curl_setopt_array($curl, $arrayCurl);

        $response = curl_exec($curl);

        curl_close($curl);
        return  $response;
    }
    /**
     * listUser muestra todos los usuarios por medio del api
     * @access public
     * @return string
     */
    public function listUser()
    {
        $r = $this->request( array() , "customers" , "GET");
        return $r;
    }
    /**
     * createUser crea json para enviar a vimeo y crear api
     * 
     * @access public
     * @return void
     */
    public function createUser($user_id)
    {
        $user = get_user_by('id', $user_id);
        $json = array(
            "name"      => $user->data->user_login ,
            "email"     => $user->data->user_email ,
            "product"   => get_option( 'vimeoApy_productId' ) ,
            "plan"      => "standard" ,
        );

        $r = $this->request( $json , "customers" );
        update_user_meta($user_id,"VimeoApySend",json_encode($json));
        update_user_meta($user_id,"VimeoApyResult",$r);

        $r = json_decode($r);

        return array(
            "send"      => $json,
            "respod"    => $r,
        );
    }
    /**
     * listProduct muestra los productos por medio del api
     * 
     * @access public
     * @return string
     */
    public function listProduct()
    {
        $r = $this->request( array() , "products" , "GET");
        return $r;
    }
    /**
     * addProduct agrega el producto al usuario
     * 
     * @access public
     * @return void
     */
    public function addProduct($user_id)
    {
        $json = array(
            'product' => get_option( 'vimeoApy_productId' ),
            'href' => get_option( 'vimeoApy_productId' ),
            'plan' => 'standard',
        );
        $VimeoApyResult = get_user_meta($user_id,"VimeoApyResult",true);
        $VimeoApyResult = json_decode($VimeoApyResult,true);

        $user_id_api = $VimeoApyResult['id'];

        $r = $this->request( $json , "customers/$user_id_api/products" , "PUT");
        return $r;
    }
    /**
     * removeProduct elimina el producto al usuario
     * 
     * @access public
     * @return void
     */
    public function removeProduct($user_id)
    {
        
        $json = array(
            'product' => get_option( 'vimeoApy_productId' ),
        );
        $VimeoApyResult = get_user_meta($user_id,"VimeoApyResult",true);
        $VimeoApyResult = json_decode($VimeoApyResult,true);

        $user_id_api = $VimeoApyResult['id'];

        $r = $this->request($json , "customers/$user_id_api/products" , "DELETE");
        $r =  json_decode($r,true);
        return $r;
    }
}