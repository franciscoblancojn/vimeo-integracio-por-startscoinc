<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');

$data = json_decode(file_get_contents('php://input'), true);
if(isset($data)){
    $_POST = $data;
}


VISS_validateField($_POST , array('action','data')); 

$data = $_POST['data'];
switch ($_POST['action']) {
    case 'createUser':
        VISS_validateField($data, array('email','password','name'));
        $r  = VISS_createUSer($data);
        VISS_result(array(
            "res"   => "ok",
            "msj"   => "UsuarioCreado",
            "data"  => $r
        ));
        break;
    case 'deleteUser':
        VISS_validateField($data, array('user_id'));
        $r  = VISS_deleteUser($data);
        VISS_result(array(
            "res"   => "ok",
            "msj"   => "Usuario Eliminado",
            "data"  => $r
        ));
        break;
}