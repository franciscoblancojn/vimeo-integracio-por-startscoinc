<?php
/*
Plugin Name: Vimeo Integracio Por Startscoinc
Plugin URI: https://startscoinc.com/es/
Description: Integra Vimeo para manejar el login y las suscripciones
Author: Startsco
Version: 0.1
Author URI: https://startscoinc.com/es/#
License: 
*/
/**
 * Require admin
 * Is field for requiere fields
 */
define("prefix_VISS","VISS");
define("VISS_location",plugin_dir_path( __FILE__ ));
define("VISS_url",plugin_dir_url( __FILE__ ));

require_once plugin_dir_path( __FILE__ ) . 'functions.php';
require_once plugin_dir_path( __FILE__ ) . 'api.php';
require_once plugin_dir_path( __FILE__ ) . 'hook.php';
require_once plugin_dir_path( __FILE__ ) . 'shortcode.php';
require_once plugin_dir_path( __FILE__ ) . 'optionPage.php';