<?php
function VISS_result($json = array())
{
    echo json_encode($json);
    exit;
}
function VISS_validateField($data = array(), $keys = array())
{
    for ($i=0; $i < count($keys); $i++) { 
        if(!isset($data[$keys[$i]]) || $data[$keys[$i]] == null || $data[$keys[$i]] == ""){
            VISS_result(array(
                "res"   => "error",
                "msj"   => $keys[$i]." undefined",
            ));
        }
    }
}
function VISS_createUSer($data = array())
{
    $user = get_user_by( 'email', $data['email'] );
    if($user === false){
        $r = wp_create_user( $data['name'] , $data['password'], $data['email'] );
        $user = get_user_by( 'email', $data['email'] );
    }

    $api = new VISS_api();
    $r = $api->createUser($user->ID);
    update_user_meta($user->ID,'userVISS',"yes");
    return array(
        "user"          => $user,
        "respondApy"    => $r
    );
}
function VISS_deleteUser($data)
{
    $user_id = $data['user_id'];
    //delete user in api

    //delete user in worpdress
    $respondWordpress = wp_delete_user( intval($user_id) );
    return array(
        "respondWordpres"   => $respondWordpress,
        "respondApy"        => ""
    );
}
function VISS_addProduct($user_id)
{
    $api = new VISS_api();
    $r = $api->addProduct($user_id);
    update_user_meta($user_id,'addProduct',$r);
    return array(
        "respondApy"    => $r
    );
}
function VISS_removeProduct($user_id)
{
    $api = new VISS_api();
    $r = $api->removeProduct($user_id);
    update_user_meta($user_id,'removeProduct',$r);
    return array(
        "respondApy"    => $r
    );
}