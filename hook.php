<?php
function VISS_order_processing($order_id) {
    $order = wc_get_order( $order_id );
    $r  = VISS_createUSer(array(
            'email'     => $order->get_billing_email(),
            'password'  => "",
            'name'      => $order->get_billing_first_name()." ".$order->get_billing_last_name()
        )
    );
    $user_id = $r['user']->ID;
    $r  = VISS_addProduct($user_id);
}
add_action('woocommerce_order_status_processing',   'VISS_order_processing' , 10, 1); 

function VISS_order_failed($order_id) {
    $order = wc_get_order( $order_id );
    $r  = VISS_createUSer(array(
            'email'     => $order->get_billing_email(),
            'password'  => "",
            'name'      => $order->get_billing_first_name()." ".$order->get_billing_last_name()
        )
    );
    $user_id = $r['user']->ID;
    $r  = VISS_removeProduct($user_id);
}
add_action('woocommerce_order_status_failed',   'VISS_order_failed' , 10, 1); 