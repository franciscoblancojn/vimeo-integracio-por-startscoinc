<?php
add_action( 'admin_menu', 'VISS_option_page' );
 
function VISS_option_page() {
	add_menu_page(
		'Vimeo Apy', // page <title>Title</title>
		'Vimeo Apy', // menu link text
		'manage_options', // capability to access the page
		'vimeoApy-slug', // page URL slug
		'admin_vimeoApy', // callback function /w content
		'dashicons-welcome-write-blog', // menu icon
		5 // priority
	);
 
}
add_action( 'admin_init',  'VISS_register_setting' );
 
function VISS_register_setting(){
 
	register_setting(
		'vimeoApy_settings', // settings group name
		'vimeoApy_apyKey', // option name
		'sanitize_text_field' // sanitization function
	);
 
	add_settings_section(
		'vimeoApy_settings_section_id', // section ID
		'', // title (if needed)
		'', // callback function (if needed)
		'vimeoApy-slug' // page slug
	);
 
	add_settings_field(
		'vimeoApy_apyKey',
		'API KEY',
		'vimeoApy_text_field_html', // function which prints the field
		'vimeoApy-slug', // page slug
		'vimeoApy_settings_section_id', // section ID
		array( 
            'label_for' => 'vimeoApy_apyKey'
		)
    );
 
    register_setting(
		'vimeoApy_settings', // settings group name
		'vimeoApy_productId', // option name
		'sanitize_text_field' // sanitization function
	);
	add_settings_field(
		'vimeoApy_productId',
		'Product Id',
		'vimeoApy_productId_f', // function which prints the field
		'vimeoApy-slug', // page slug
		'vimeoApy_settings_section_id', // section ID
		array( 
            'label_for' => 'vimeoApy_productId'
		)
    );
}
function vimeoApy_text_field_html(){
	$text = get_option( 'vimeoApy_apyKey' );
	printf(
		'<input type="text" id="vimeoApy_apyKey" name="vimeoApy_apyKey" value="%s" />',
		esc_attr( $text )
	);
 
}
function vimeoApy_productId_f(){
	$text = get_option( 'vimeoApy_productId' );
	printf(
		'<input type="text" id="vimeoApy_productId" name="vimeoApy_productId" value="%s" />',
		esc_attr( $text )
	);
}
function admin_vimeoApy(){
    ?>
    <div class="wrap">
        <h1>
            Vimeo Apy
        </h1>
	    <form method="post" action="options.php">
            <?php
                settings_fields( 'vimeoApy_settings' ); 
                do_settings_sections( 'vimeoApy-slug' ); 
                submit_button();
            ?>
	    </form>
		<?php
			require_once VISS_location . 'adminApi.php';
		?>
    </div>
    <?php
}