<h1>
    Create User
</h1>
<p>
    Please note that users you create manually will not be charged
</p>
<table>
    <tr>
        <td>
            <input type="email" name="email" id="email" placeholder="Email">
        </td>
        <td>
            <input type="text" name="name" id="user" placeholder="Name">
        </td>
        <td>
            <input type="password" name="password" id="password" placeholder="Password">
        </td>
        <td>
            <button id="createUser" class="button button-primary">Create</button>
        </td>
    </tr>
</table>
<script>
    email =  document.getElementById('email')
    user =  document.getElementById('user')
    password =  document.getElementById('password')
    createUser =  document.getElementById('createUser')
    createUser.onclick = () => {
        json = {
            action  : "createUser",
            data    : {
                email : email.value,
                name : user.value,
                password : password.value,
            }
        }
        
        request(json,(result) => {
            showResult(result)
            //clear inputs
                email.value = ""
                user.value = ""
                password.value = ""
        })
    }
    showResult = (result) => {
        result = JSON.parse(result)
        console.log("result",result);
        alert(result.res+"\n"+result.msj)
    }
    request = (json = {} , res = (result) => {console.log(result)}) => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        console.log("send",json);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(json),
            redirect: 'follow'
        };

        fetch("<?=VISS_url?>action.php", requestOptions)
        .then(response => response.text())
        .then(result => res(result))
        .catch(error => console.log('error', error));
    }
</script>

<h1>
    Add Productos
</h1>
<?php
if(isset($_POST['userAddSelect'])){
    //delete apy
    $r  = VISS_addProduct($_POST['userAddSelect']);
}
$users = get_users(array(
    'meta_key'     => 'userVISS',
    'meta_value'   => 'yes',
    'meta_compare' => '==',
));
$options = "<option default selected disabled value=''>Select</option>";
for ($i=0; $i < count($users); $i++) { 
    $user = $users[$i];
    $user_id = $user->ID;
    $data = get_userdata( $user_id );
    $options .= "<option value='$user_id' >$data->user_email</option>";

}
?>
<form action="" method="POST">
    <table>
        <tr>
            <td>
                <select name="userAddSelect" id="userAddSelect"><?=$options?></select>
            </td>
            <td>
                <button id="userAddBtn" class="button button-primary">
                    Add
                </button>
            </td>
        </tr>
    </table>
</form>

<h1>
    Remove Productos
</h1>
<?php
if(isset($_POST['userDeleteSelect'])){
    //delete apy
    $r  = VISS_removeProduct($_POST['userDeleteSelect']);
}
$users = get_users(array(
    'meta_key'     => 'userVISS',
    'meta_value'   => 'yes',
    'meta_compare' => '==',
));
$options = "<option default selected disabled value=''>Select</option>";
for ($i=0; $i < count($users); $i++) { 
    $user = $users[$i];
    $user_id = $user->ID;
    $data = get_userdata( $user_id );
    $options .= "<option value='$user_id' >$data->user_email</option>";

}
?>
<form action="" method="POST">
    <table>
        <tr>
            <td>
                <select name="userDeleteSelect" id="userDeleteSelect"><?=$options?></select>
            </td>
            <td>
                <button id="userDeleteBtn" class="button button-primary">
                    Remove
                </button>
            </td>
        </tr>
    </table>
</form>